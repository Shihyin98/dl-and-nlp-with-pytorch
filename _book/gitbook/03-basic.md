# Ch3- Basic with PyTorch

# Outline
**1. 張量 (Tensor)**
**2. 數學操作**
**3. 數理統計**
**4. 比較操作**
<br>

~ ~ ~ ~ ~ :blush:

* 了解 PyTorch的基礎知識

------
# 1. 張量 (Tensor)
## 1.1- Knowledge
* 一種幾何實體，廣義上的「數量」。
* 概念：
  1. 標量
  2. 向量 (Vectors)
  3. 線性映射
* 用 **座標系統** 表達，作記「標量的陣列」。
* Example: 
  * pt01-tensor.py
    * torch.rand() #隨機
    * torch.andd() #相加
  * pt02-tensor.py
    * np.ones() #創建一個 Array
    * np.add()
    * torch.form_numpy()  #numpy轉 Tensor 
<br>

## 1.2- Note
* 矩陣當中，到底行，列的方向是什麼？[[2]](-)
  | def  |  英文  |   台灣 |  大陸  |
  | :--: |  :---: |  :---: | :---: |
  | row  |  橫的  |   列   |   行   |
  |column|  直的  |   行   |   列   |
  |  -   |   -    |直行橫列|直列橫行|
* 由於中文實在是太混亂，所以**一律採用英文 row, column**。

### 1.2.1- Definition
* 張量 (Tensor)[[2]](-)
  * 第零階張量 （r=0） 為純量
    第一階張量 （r=1） 為向量 
    第二階張量 （r=2） 為矩陣
  * 是一个定义在一些**向量空间**和一些**对偶空间**的**笛卡儿积**上的多重线性映射，其坐标是|n|维空间内，有|n|个分量的一种量， 其中每个分量都是坐标的函数， 而在坐标变换时，这些分量也依照某些规则作线性变换。
  r 称为该张量的秩或阶（与矩阵的秩和阶均无关系）。
------
# 2. 數學操作
## 2.1- Knowledge
* 數學函數： add()、abs()、randn()
* Example: 
  * pt03-math.py
    * torch.abs()  #絕對值
    * torch.acos() #反餘弦
    * torch.add() #加標量
<br>

------
# 3. 數理統計
* 統計函數： mean()
* Example: 
  * pt04-math.py
    * torch.mean()  #均值
<br>

------
# 4. 比較操作
* 比較操作函數
* Example: 
  * pt05-operator.py
    * torch.eq() #相等性：比較個元素
    * torch.equal() #相等性：相同形狀、元素值
    * torch.ge() #比大小：大於等於
    * torch.gt() #比大小：大於
<br>

-----
# Reference
[[1] [筆記] numpy用法(1) 宣告與基本運算_陳雲濤的部落格_2017](http://violin-tao.blogspot.com/2017/05/numpy-1.html)<br>
[[2] 張量 (Tensor)_陳鍾誠的網站](http://ccckmit.wikidot.com/ca:tensor)

