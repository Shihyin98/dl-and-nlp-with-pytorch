## Outline
* Ch00-Introduction
* Ch01-DL Synopsis
* Ch02-PyTorch Installation
* Ch03-PyTorch Basis
* Ch04-Example Practice
* Ch05-Feedforward NN
* Ch06-Visual Tool: Visdom
* Ch07-CNN
* Ch08-RNN
* Ch09-AutoEncoder
* Ch10-GAN
* Ch11-Seq2seq
* Ch12-Application

~ ~ ~ ~ ~ :blush:








